// @SOURCE:/Users/dtn29/Work/Airisumi/Source/Server/play/Dev/conf/routes
// @HASH:f5d72bf37480d98d91dbfd1e42d653893b412e1e
// @DATE:Fri May 02 02:46:11 EDT 2014

package controllers;

public class routes {
public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets();
public static final controllers.ReverseUsers Users = new controllers.ReverseUsers();
public static final controllers.ReverseComments Comments = new controllers.ReverseComments();
public static final controllers.ReverseVotes Votes = new controllers.ReverseVotes();
public static final controllers.ReverseItemAttributes ItemAttributes = new controllers.ReverseItemAttributes();
public static final controllers.ReverseComparisons Comparisons = new controllers.ReverseComparisons();
public static final controllers.ReverseApplication Application = new controllers.ReverseApplication();
public static final controllers.ReverseNotifications Notifications = new controllers.ReverseNotifications();
public static final controllers.ReverseItems Items = new controllers.ReverseItems();
public static class javascript {
public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets();
public static final controllers.javascript.ReverseUsers Users = new controllers.javascript.ReverseUsers();
public static final controllers.javascript.ReverseComments Comments = new controllers.javascript.ReverseComments();
public static final controllers.javascript.ReverseVotes Votes = new controllers.javascript.ReverseVotes();
public static final controllers.javascript.ReverseItemAttributes ItemAttributes = new controllers.javascript.ReverseItemAttributes();
public static final controllers.javascript.ReverseComparisons Comparisons = new controllers.javascript.ReverseComparisons();
public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication();
public static final controllers.javascript.ReverseNotifications Notifications = new controllers.javascript.ReverseNotifications();
public static final controllers.javascript.ReverseItems Items = new controllers.javascript.ReverseItems();
}
public static class ref {
public static final controllers.ref.ReverseAssets Assets = new controllers.ref.ReverseAssets();
public static final controllers.ref.ReverseUsers Users = new controllers.ref.ReverseUsers();
public static final controllers.ref.ReverseComments Comments = new controllers.ref.ReverseComments();
public static final controllers.ref.ReverseVotes Votes = new controllers.ref.ReverseVotes();
public static final controllers.ref.ReverseItemAttributes ItemAttributes = new controllers.ref.ReverseItemAttributes();
public static final controllers.ref.ReverseComparisons Comparisons = new controllers.ref.ReverseComparisons();
public static final controllers.ref.ReverseApplication Application = new controllers.ref.ReverseApplication();
public static final controllers.ref.ReverseNotifications Notifications = new controllers.ref.ReverseNotifications();
public static final controllers.ref.ReverseItems Items = new controllers.ref.ReverseItems();
}
}
          