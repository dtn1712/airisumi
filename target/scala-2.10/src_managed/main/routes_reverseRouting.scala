// @SOURCE:/Users/dtn29/Work/Airisumi/Source/Server/play/Dev/conf/routes
// @HASH:f5d72bf37480d98d91dbfd1e42d653893b412e1e
// @DATE:Fri May 02 02:46:11 EDT 2014

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString


// @LINE:64
// @LINE:60
// @LINE:59
// @LINE:58
// @LINE:57
// @LINE:56
// @LINE:55
// @LINE:52
// @LINE:51
// @LINE:50
// @LINE:49
// @LINE:48
// @LINE:46
// @LINE:45
// @LINE:44
// @LINE:43
// @LINE:42
// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:37
// @LINE:36
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:7
// @LINE:6
package controllers {

// @LINE:64
class ReverseAssets {
    

// @LINE:64
def at(file:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                                                
    
}
                          

// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
class ReverseUsers {
    

// @LINE:12
def create(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/users")
}
                                                

// @LINE:13
def follow(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/users/follow")
}
                                                

// @LINE:11
def settings(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/users/settings")
}
                                                

// @LINE:10
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/users")
}
                                                

// @LINE:14
def unfollow(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/users/unfollow")
}
                                                

// @LINE:16
def update(id:String): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/users/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:15
def get(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/users/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                
    
}
                          

// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:37
// @LINE:36
class ReverseComments {
    

// @LINE:38
def create(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/comments")
}
                                                

// @LINE:36
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/comments")
}
                                                

// @LINE:40
def delete(id:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/comments/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:39
def update(id:String): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/comments/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:37
def get(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/comments/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                
    
}
                          

// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
class ReverseVotes {
    

// @LINE:32
def create(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/votes")
}
                                                

// @LINE:30
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/votes")
}
                                                

// @LINE:34
def delete(id:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/votes/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:33
def update(id:String): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/votes/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:31
def get(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/votes/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                
    
}
                          

// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:25
// @LINE:24
class ReverseItemAttributes {
    

// @LINE:26
def create(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/itemAttributes")
}
                                                

// @LINE:24
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/itemAttributes")
}
                                                

// @LINE:28
def delete(id:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/itemAttributes/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:27
def update(id:String): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/itemAttributes/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:25
def get(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/itemAttributes/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                
    
}
                          

// @LINE:60
// @LINE:59
// @LINE:58
// @LINE:57
// @LINE:56
// @LINE:55
// @LINE:52
// @LINE:51
// @LINE:50
// @LINE:49
// @LINE:48
class ReverseComparisons {
    

// @LINE:56
def getVotes(comparisonId:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/comparisons/" + implicitly[PathBindable[String]].unbind("comparisonId", dynamicString(comparisonId)) + "/votes")
}
                                                

// @LINE:50
def create(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/comparisons")
}
                                                

// @LINE:58
def getComment(comparisonId:String, commentId:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/comparisons/" + implicitly[PathBindable[String]].unbind("comparisonId", dynamicString(comparisonId)) + "/comments/" + implicitly[PathBindable[String]].unbind("commentId", dynamicString(commentId)))
}
                                                

// @LINE:60
def getItemAttribute(comparisonId:String, itemAttributeId:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/comparisons/" + implicitly[PathBindable[String]].unbind("comparisonId", dynamicString(comparisonId)) + "/itemAttributes/" + implicitly[PathBindable[String]].unbind("itemAttributeId", dynamicString(itemAttributeId)))
}
                                                

// @LINE:48
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/comparisons")
}
                                                

// @LINE:52
def delete(id:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/comparisons/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:55
def getComments(comparisonId:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/comparisons/" + implicitly[PathBindable[String]].unbind("comparisonId", dynamicString(comparisonId)) + "/comments")
}
                                                

// @LINE:57
def getItemAttributes(comparisonId:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/comparisons/" + implicitly[PathBindable[String]].unbind("comparisonId", dynamicString(comparisonId)) + "/itemAttributes")
}
                                                

// @LINE:51
def update(id:String): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/comparisons/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:49
def get(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/comparisons/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:59
def getVote(comparisonId:String, voteId:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/comparisons/" + implicitly[PathBindable[String]].unbind("comparisonId", dynamicString(comparisonId)) + "/votes/" + implicitly[PathBindable[String]].unbind("voteId", dynamicString(voteId)))
}
                                                
    
}
                          

// @LINE:7
// @LINE:6
class ReverseApplication {
    

// @LINE:7
def upload(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "upload")
}
                                                

// @LINE:6
def index(): Call = {
   Call("GET", _prefix)
}
                                                
    
}
                          

// @LINE:46
// @LINE:45
// @LINE:44
// @LINE:43
// @LINE:42
class ReverseNotifications {
    

// @LINE:44
def create(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/notifications")
}
                                                

// @LINE:42
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/notifications")
}
                                                

// @LINE:46
def delete(id:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/notifications/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:45
def update(id:String): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/notifications/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:43
def get(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/notifications/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                
    
}
                          

// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
class ReverseItems {
    

// @LINE:20
def create(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/items")
}
                                                

// @LINE:18
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/items")
}
                                                

// @LINE:22
def delete(id:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/items/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:21
def update(id:String): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/items/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:19
def get(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/items/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                
    
}
                          
}
                  


// @LINE:64
// @LINE:60
// @LINE:59
// @LINE:58
// @LINE:57
// @LINE:56
// @LINE:55
// @LINE:52
// @LINE:51
// @LINE:50
// @LINE:49
// @LINE:48
// @LINE:46
// @LINE:45
// @LINE:44
// @LINE:43
// @LINE:42
// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:37
// @LINE:36
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:7
// @LINE:6
package controllers.javascript {

// @LINE:64
class ReverseAssets {
    

// @LINE:64
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        
    
}
              

// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
class ReverseUsers {
    

// @LINE:12
def create : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Users.create",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users"})
      }
   """
)
                        

// @LINE:13
def follow : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Users.follow",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/follow"})
      }
   """
)
                        

// @LINE:11
def settings : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Users.settings",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/settings"})
      }
   """
)
                        

// @LINE:10
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Users.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users"})
      }
   """
)
                        

// @LINE:14
def unfollow : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Users.unfollow",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/unfollow"})
      }
   """
)
                        

// @LINE:16
def update : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Users.update",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:15
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Users.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        
    
}
              

// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:37
// @LINE:36
class ReverseComments {
    

// @LINE:38
def create : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comments.create",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comments"})
      }
   """
)
                        

// @LINE:36
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comments.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comments"})
      }
   """
)
                        

// @LINE:40
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comments.delete",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comments/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:39
def update : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comments.update",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comments/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:37
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comments.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comments/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        
    
}
              

// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
class ReverseVotes {
    

// @LINE:32
def create : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Votes.create",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/votes"})
      }
   """
)
                        

// @LINE:30
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Votes.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/votes"})
      }
   """
)
                        

// @LINE:34
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Votes.delete",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/votes/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:33
def update : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Votes.update",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/votes/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:31
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Votes.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/votes/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        
    
}
              

// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:25
// @LINE:24
class ReverseItemAttributes {
    

// @LINE:26
def create : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ItemAttributes.create",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/itemAttributes"})
      }
   """
)
                        

// @LINE:24
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ItemAttributes.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/itemAttributes"})
      }
   """
)
                        

// @LINE:28
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ItemAttributes.delete",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/itemAttributes/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:27
def update : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ItemAttributes.update",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/itemAttributes/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:25
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ItemAttributes.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/itemAttributes/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        
    
}
              

// @LINE:60
// @LINE:59
// @LINE:58
// @LINE:57
// @LINE:56
// @LINE:55
// @LINE:52
// @LINE:51
// @LINE:50
// @LINE:49
// @LINE:48
class ReverseComparisons {
    

// @LINE:56
def getVotes : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comparisons.getVotes",
   """
      function(comparisonId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comparisons/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("comparisonId", encodeURIComponent(comparisonId)) + "/votes"})
      }
   """
)
                        

// @LINE:50
def create : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comparisons.create",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comparisons"})
      }
   """
)
                        

// @LINE:58
def getComment : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comparisons.getComment",
   """
      function(comparisonId,commentId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comparisons/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("comparisonId", encodeURIComponent(comparisonId)) + "/comments/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("commentId", encodeURIComponent(commentId))})
      }
   """
)
                        

// @LINE:60
def getItemAttribute : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comparisons.getItemAttribute",
   """
      function(comparisonId,itemAttributeId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comparisons/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("comparisonId", encodeURIComponent(comparisonId)) + "/itemAttributes/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("itemAttributeId", encodeURIComponent(itemAttributeId))})
      }
   """
)
                        

// @LINE:48
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comparisons.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comparisons"})
      }
   """
)
                        

// @LINE:52
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comparisons.delete",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comparisons/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:55
def getComments : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comparisons.getComments",
   """
      function(comparisonId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comparisons/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("comparisonId", encodeURIComponent(comparisonId)) + "/comments"})
      }
   """
)
                        

// @LINE:57
def getItemAttributes : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comparisons.getItemAttributes",
   """
      function(comparisonId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comparisons/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("comparisonId", encodeURIComponent(comparisonId)) + "/itemAttributes"})
      }
   """
)
                        

// @LINE:51
def update : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comparisons.update",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comparisons/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:49
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comparisons.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comparisons/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:59
def getVote : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Comparisons.getVote",
   """
      function(comparisonId,voteId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comparisons/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("comparisonId", encodeURIComponent(comparisonId)) + "/votes/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("voteId", encodeURIComponent(voteId))})
      }
   """
)
                        
    
}
              

// @LINE:7
// @LINE:6
class ReverseApplication {
    

// @LINE:7
def upload : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.upload",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "upload"})
      }
   """
)
                        

// @LINE:6
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
   """
)
                        
    
}
              

// @LINE:46
// @LINE:45
// @LINE:44
// @LINE:43
// @LINE:42
class ReverseNotifications {
    

// @LINE:44
def create : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Notifications.create",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/notifications"})
      }
   """
)
                        

// @LINE:42
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Notifications.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/notifications"})
      }
   """
)
                        

// @LINE:46
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Notifications.delete",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/notifications/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:45
def update : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Notifications.update",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/notifications/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:43
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Notifications.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/notifications/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        
    
}
              

// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
class ReverseItems {
    

// @LINE:20
def create : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Items.create",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/items"})
      }
   """
)
                        

// @LINE:18
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Items.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/items"})
      }
   """
)
                        

// @LINE:22
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Items.delete",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/items/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:21
def update : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Items.update",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/items/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:19
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Items.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/items/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        
    
}
              
}
        


// @LINE:64
// @LINE:60
// @LINE:59
// @LINE:58
// @LINE:57
// @LINE:56
// @LINE:55
// @LINE:52
// @LINE:51
// @LINE:50
// @LINE:49
// @LINE:48
// @LINE:46
// @LINE:45
// @LINE:44
// @LINE:43
// @LINE:42
// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:37
// @LINE:36
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:7
// @LINE:6
package controllers.ref {


// @LINE:64
class ReverseAssets {
    

// @LINE:64
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /assets URL path""", _prefix + """assets/$file<.+>""")
)
                      
    
}
                          

// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
class ReverseUsers {
    

// @LINE:12
def create(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Users.create(), HandlerDef(this, "controllers.Users", "create", Seq(), "POST", """""", _prefix + """api/users""")
)
                      

// @LINE:13
def follow(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Users.follow(), HandlerDef(this, "controllers.Users", "follow", Seq(), "POST", """""", _prefix + """api/users/follow""")
)
                      

// @LINE:11
def settings(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Users.settings(), HandlerDef(this, "controllers.Users", "settings", Seq(), "POST", """""", _prefix + """api/users/settings""")
)
                      

// @LINE:10
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Users.list(), HandlerDef(this, "controllers.Users", "list", Seq(), "GET", """ API""", _prefix + """api/users""")
)
                      

// @LINE:14
def unfollow(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Users.unfollow(), HandlerDef(this, "controllers.Users", "unfollow", Seq(), "POST", """""", _prefix + """api/users/unfollow""")
)
                      

// @LINE:16
def update(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Users.update(id), HandlerDef(this, "controllers.Users", "update", Seq(classOf[String]), "PUT", """""", _prefix + """api/users/$id<[^/]+>""")
)
                      

// @LINE:15
def get(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Users.get(id), HandlerDef(this, "controllers.Users", "get", Seq(classOf[String]), "GET", """""", _prefix + """api/users/$id<[^/]+>""")
)
                      
    
}
                          

// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:37
// @LINE:36
class ReverseComments {
    

// @LINE:38
def create(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comments.create(), HandlerDef(this, "controllers.Comments", "create", Seq(), "POST", """""", _prefix + """api/comments""")
)
                      

// @LINE:36
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comments.list(), HandlerDef(this, "controllers.Comments", "list", Seq(), "GET", """""", _prefix + """api/comments""")
)
                      

// @LINE:40
def delete(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comments.delete(id), HandlerDef(this, "controllers.Comments", "delete", Seq(classOf[String]), "DELETE", """""", _prefix + """api/comments/$id<[^/]+>""")
)
                      

// @LINE:39
def update(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comments.update(id), HandlerDef(this, "controllers.Comments", "update", Seq(classOf[String]), "PUT", """""", _prefix + """api/comments/$id<[^/]+>""")
)
                      

// @LINE:37
def get(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comments.get(id), HandlerDef(this, "controllers.Comments", "get", Seq(classOf[String]), "GET", """""", _prefix + """api/comments/$id<[^/]+>""")
)
                      
    
}
                          

// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
class ReverseVotes {
    

// @LINE:32
def create(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Votes.create(), HandlerDef(this, "controllers.Votes", "create", Seq(), "POST", """""", _prefix + """api/votes""")
)
                      

// @LINE:30
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Votes.list(), HandlerDef(this, "controllers.Votes", "list", Seq(), "GET", """""", _prefix + """api/votes""")
)
                      

// @LINE:34
def delete(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Votes.delete(id), HandlerDef(this, "controllers.Votes", "delete", Seq(classOf[String]), "DELETE", """""", _prefix + """api/votes/$id<[^/]+>""")
)
                      

// @LINE:33
def update(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Votes.update(id), HandlerDef(this, "controllers.Votes", "update", Seq(classOf[String]), "PUT", """""", _prefix + """api/votes/$id<[^/]+>""")
)
                      

// @LINE:31
def get(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Votes.get(id), HandlerDef(this, "controllers.Votes", "get", Seq(classOf[String]), "GET", """""", _prefix + """api/votes/$id<[^/]+>""")
)
                      
    
}
                          

// @LINE:28
// @LINE:27
// @LINE:26
// @LINE:25
// @LINE:24
class ReverseItemAttributes {
    

// @LINE:26
def create(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.ItemAttributes.create(), HandlerDef(this, "controllers.ItemAttributes", "create", Seq(), "POST", """""", _prefix + """api/itemAttributes""")
)
                      

// @LINE:24
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.ItemAttributes.list(), HandlerDef(this, "controllers.ItemAttributes", "list", Seq(), "GET", """""", _prefix + """api/itemAttributes""")
)
                      

// @LINE:28
def delete(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.ItemAttributes.delete(id), HandlerDef(this, "controllers.ItemAttributes", "delete", Seq(classOf[String]), "DELETE", """""", _prefix + """api/itemAttributes/$id<[^/]+>""")
)
                      

// @LINE:27
def update(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.ItemAttributes.update(id), HandlerDef(this, "controllers.ItemAttributes", "update", Seq(classOf[String]), "PUT", """""", _prefix + """api/itemAttributes/$id<[^/]+>""")
)
                      

// @LINE:25
def get(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.ItemAttributes.get(id), HandlerDef(this, "controllers.ItemAttributes", "get", Seq(classOf[String]), "GET", """""", _prefix + """api/itemAttributes/$id<[^/]+>""")
)
                      
    
}
                          

// @LINE:60
// @LINE:59
// @LINE:58
// @LINE:57
// @LINE:56
// @LINE:55
// @LINE:52
// @LINE:51
// @LINE:50
// @LINE:49
// @LINE:48
class ReverseComparisons {
    

// @LINE:56
def getVotes(comparisonId:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comparisons.getVotes(comparisonId), HandlerDef(this, "controllers.Comparisons", "getVotes", Seq(classOf[String]), "GET", """""", _prefix + """api/comparisons/$comparisonId<[^/]+>/votes""")
)
                      

// @LINE:50
def create(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comparisons.create(), HandlerDef(this, "controllers.Comparisons", "create", Seq(), "POST", """""", _prefix + """api/comparisons""")
)
                      

// @LINE:58
def getComment(comparisonId:String, commentId:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comparisons.getComment(comparisonId, commentId), HandlerDef(this, "controllers.Comparisons", "getComment", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """api/comparisons/$comparisonId<[^/]+>/comments/$commentId<[^/]+>""")
)
                      

// @LINE:60
def getItemAttribute(comparisonId:String, itemAttributeId:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comparisons.getItemAttribute(comparisonId, itemAttributeId), HandlerDef(this, "controllers.Comparisons", "getItemAttribute", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """api/comparisons/$comparisonId<[^/]+>/itemAttributes/$itemAttributeId<[^/]+>""")
)
                      

// @LINE:48
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comparisons.list(), HandlerDef(this, "controllers.Comparisons", "list", Seq(), "GET", """""", _prefix + """api/comparisons""")
)
                      

// @LINE:52
def delete(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comparisons.delete(id), HandlerDef(this, "controllers.Comparisons", "delete", Seq(classOf[String]), "DELETE", """""", _prefix + """api/comparisons/$id<[^/]+>""")
)
                      

// @LINE:55
def getComments(comparisonId:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comparisons.getComments(comparisonId), HandlerDef(this, "controllers.Comparisons", "getComments", Seq(classOf[String]), "GET", """ Relation of Comparison""", _prefix + """api/comparisons/$comparisonId<[^/]+>/comments""")
)
                      

// @LINE:57
def getItemAttributes(comparisonId:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comparisons.getItemAttributes(comparisonId), HandlerDef(this, "controllers.Comparisons", "getItemAttributes", Seq(classOf[String]), "GET", """""", _prefix + """api/comparisons/$comparisonId<[^/]+>/itemAttributes""")
)
                      

// @LINE:51
def update(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comparisons.update(id), HandlerDef(this, "controllers.Comparisons", "update", Seq(classOf[String]), "PUT", """""", _prefix + """api/comparisons/$id<[^/]+>""")
)
                      

// @LINE:49
def get(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comparisons.get(id), HandlerDef(this, "controllers.Comparisons", "get", Seq(classOf[String]), "GET", """""", _prefix + """api/comparisons/$id<[^/]+>""")
)
                      

// @LINE:59
def getVote(comparisonId:String, voteId:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Comparisons.getVote(comparisonId, voteId), HandlerDef(this, "controllers.Comparisons", "getVote", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """api/comparisons/$comparisonId<[^/]+>/votes/$voteId<[^/]+>""")
)
                      
    
}
                          

// @LINE:7
// @LINE:6
class ReverseApplication {
    

// @LINE:7
def upload(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.upload(), HandlerDef(this, "controllers.Application", "upload", Seq(), "POST", """""", _prefix + """upload""")
)
                      

// @LINE:6
def index(any:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.index(any), HandlerDef(this, "controllers.Application", "index", Seq(classOf[String]), "GET", """ Home page""", _prefix + """""")
)
                      
    
}
                          

// @LINE:46
// @LINE:45
// @LINE:44
// @LINE:43
// @LINE:42
class ReverseNotifications {
    

// @LINE:44
def create(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Notifications.create(), HandlerDef(this, "controllers.Notifications", "create", Seq(), "POST", """""", _prefix + """api/notifications""")
)
                      

// @LINE:42
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Notifications.list(), HandlerDef(this, "controllers.Notifications", "list", Seq(), "GET", """""", _prefix + """api/notifications""")
)
                      

// @LINE:46
def delete(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Notifications.delete(id), HandlerDef(this, "controllers.Notifications", "delete", Seq(classOf[String]), "DELETE", """""", _prefix + """api/notifications/$id<[^/]+>""")
)
                      

// @LINE:45
def update(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Notifications.update(id), HandlerDef(this, "controllers.Notifications", "update", Seq(classOf[String]), "PUT", """""", _prefix + """api/notifications/$id<[^/]+>""")
)
                      

// @LINE:43
def get(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Notifications.get(id), HandlerDef(this, "controllers.Notifications", "get", Seq(classOf[String]), "GET", """""", _prefix + """api/notifications/$id<[^/]+>""")
)
                      
    
}
                          

// @LINE:22
// @LINE:21
// @LINE:20
// @LINE:19
// @LINE:18
class ReverseItems {
    

// @LINE:20
def create(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Items.create(), HandlerDef(this, "controllers.Items", "create", Seq(), "POST", """""", _prefix + """api/items""")
)
                      

// @LINE:18
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Items.list(), HandlerDef(this, "controllers.Items", "list", Seq(), "GET", """""", _prefix + """api/items""")
)
                      

// @LINE:22
def delete(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Items.delete(id), HandlerDef(this, "controllers.Items", "delete", Seq(classOf[String]), "DELETE", """""", _prefix + """api/items/$id<[^/]+>""")
)
                      

// @LINE:21
def update(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Items.update(id), HandlerDef(this, "controllers.Items", "update", Seq(classOf[String]), "PUT", """""", _prefix + """api/items/$id<[^/]+>""")
)
                      

// @LINE:19
def get(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Items.get(id), HandlerDef(this, "controllers.Items", "get", Seq(classOf[String]), "GET", """""", _prefix + """api/items/$id<[^/]+>""")
)
                      
    
}
                          
}
        
    