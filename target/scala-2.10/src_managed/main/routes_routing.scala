// @SOURCE:/Users/dtn29/Work/Airisumi/Source/Server/play/Dev/conf/routes
// @HASH:f5d72bf37480d98d91dbfd1e42d653893b412e1e
// @DATE:Fri May 02 02:46:11 EDT 2014


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:6
private[this] lazy val controllers_Application_index0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
        

// @LINE:7
private[this] lazy val controllers_Application_upload1 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("upload"))))
        

// @LINE:10
private[this] lazy val controllers_Users_list2 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users"))))
        

// @LINE:11
private[this] lazy val controllers_Users_settings3 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/settings"))))
        

// @LINE:12
private[this] lazy val controllers_Users_create4 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users"))))
        

// @LINE:13
private[this] lazy val controllers_Users_follow5 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/follow"))))
        

// @LINE:14
private[this] lazy val controllers_Users_unfollow6 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/unfollow"))))
        

// @LINE:15
private[this] lazy val controllers_Users_get7 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:16
private[this] lazy val controllers_Users_update8 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:18
private[this] lazy val controllers_Items_list9 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/items"))))
        

// @LINE:19
private[this] lazy val controllers_Items_get10 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/items/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:20
private[this] lazy val controllers_Items_create11 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/items"))))
        

// @LINE:21
private[this] lazy val controllers_Items_update12 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/items/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:22
private[this] lazy val controllers_Items_delete13 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/items/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:24
private[this] lazy val controllers_ItemAttributes_list14 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/itemAttributes"))))
        

// @LINE:25
private[this] lazy val controllers_ItemAttributes_get15 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/itemAttributes/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:26
private[this] lazy val controllers_ItemAttributes_create16 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/itemAttributes"))))
        

// @LINE:27
private[this] lazy val controllers_ItemAttributes_update17 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/itemAttributes/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:28
private[this] lazy val controllers_ItemAttributes_delete18 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/itemAttributes/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:30
private[this] lazy val controllers_Votes_list19 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/votes"))))
        

// @LINE:31
private[this] lazy val controllers_Votes_get20 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/votes/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:32
private[this] lazy val controllers_Votes_create21 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/votes"))))
        

// @LINE:33
private[this] lazy val controllers_Votes_update22 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/votes/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:34
private[this] lazy val controllers_Votes_delete23 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/votes/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:36
private[this] lazy val controllers_Comments_list24 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comments"))))
        

// @LINE:37
private[this] lazy val controllers_Comments_get25 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comments/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:38
private[this] lazy val controllers_Comments_create26 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comments"))))
        

// @LINE:39
private[this] lazy val controllers_Comments_update27 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comments/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:40
private[this] lazy val controllers_Comments_delete28 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comments/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:42
private[this] lazy val controllers_Notifications_list29 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/notifications"))))
        

// @LINE:43
private[this] lazy val controllers_Notifications_get30 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/notifications/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:44
private[this] lazy val controllers_Notifications_create31 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/notifications"))))
        

// @LINE:45
private[this] lazy val controllers_Notifications_update32 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/notifications/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:46
private[this] lazy val controllers_Notifications_delete33 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/notifications/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:48
private[this] lazy val controllers_Comparisons_list34 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comparisons"))))
        

// @LINE:49
private[this] lazy val controllers_Comparisons_get35 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comparisons/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:50
private[this] lazy val controllers_Comparisons_create36 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comparisons"))))
        

// @LINE:51
private[this] lazy val controllers_Comparisons_update37 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comparisons/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:52
private[this] lazy val controllers_Comparisons_delete38 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comparisons/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:55
private[this] lazy val controllers_Comparisons_getComments39 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comparisons/"),DynamicPart("comparisonId", """[^/]+""",true),StaticPart("/comments"))))
        

// @LINE:56
private[this] lazy val controllers_Comparisons_getVotes40 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comparisons/"),DynamicPart("comparisonId", """[^/]+""",true),StaticPart("/votes"))))
        

// @LINE:57
private[this] lazy val controllers_Comparisons_getItemAttributes41 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comparisons/"),DynamicPart("comparisonId", """[^/]+""",true),StaticPart("/itemAttributes"))))
        

// @LINE:58
private[this] lazy val controllers_Comparisons_getComment42 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comparisons/"),DynamicPart("comparisonId", """[^/]+""",true),StaticPart("/comments/"),DynamicPart("commentId", """[^/]+""",true))))
        

// @LINE:59
private[this] lazy val controllers_Comparisons_getVote43 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comparisons/"),DynamicPart("comparisonId", """[^/]+""",true),StaticPart("/votes/"),DynamicPart("voteId", """[^/]+""",true))))
        

// @LINE:60
private[this] lazy val controllers_Comparisons_getItemAttribute44 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comparisons/"),DynamicPart("comparisonId", """[^/]+""",true),StaticPart("/itemAttributes/"),DynamicPart("itemAttributeId", """[^/]+""",true))))
        

// @LINE:64
private[this] lazy val controllers_Assets_at45 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
        
def documentation = List(("""GET""", prefix,"""controllers.Application.index(any:String = "none")"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """upload""","""controllers.Application.upload()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users""","""controllers.Users.list()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/settings""","""controllers.Users.settings()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users""","""controllers.Users.create()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/follow""","""controllers.Users.follow()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/unfollow""","""controllers.Users.unfollow()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/$id<[^/]+>""","""controllers.Users.get(id:String)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/$id<[^/]+>""","""controllers.Users.update(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/items""","""controllers.Items.list()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/items/$id<[^/]+>""","""controllers.Items.get(id:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/items""","""controllers.Items.create()"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/items/$id<[^/]+>""","""controllers.Items.update(id:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/items/$id<[^/]+>""","""controllers.Items.delete(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/itemAttributes""","""controllers.ItemAttributes.list()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/itemAttributes/$id<[^/]+>""","""controllers.ItemAttributes.get(id:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/itemAttributes""","""controllers.ItemAttributes.create()"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/itemAttributes/$id<[^/]+>""","""controllers.ItemAttributes.update(id:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/itemAttributes/$id<[^/]+>""","""controllers.ItemAttributes.delete(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/votes""","""controllers.Votes.list()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/votes/$id<[^/]+>""","""controllers.Votes.get(id:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/votes""","""controllers.Votes.create()"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/votes/$id<[^/]+>""","""controllers.Votes.update(id:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/votes/$id<[^/]+>""","""controllers.Votes.delete(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comments""","""controllers.Comments.list()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comments/$id<[^/]+>""","""controllers.Comments.get(id:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comments""","""controllers.Comments.create()"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comments/$id<[^/]+>""","""controllers.Comments.update(id:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comments/$id<[^/]+>""","""controllers.Comments.delete(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/notifications""","""controllers.Notifications.list()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/notifications/$id<[^/]+>""","""controllers.Notifications.get(id:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/notifications""","""controllers.Notifications.create()"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/notifications/$id<[^/]+>""","""controllers.Notifications.update(id:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/notifications/$id<[^/]+>""","""controllers.Notifications.delete(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comparisons""","""controllers.Comparisons.list()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comparisons/$id<[^/]+>""","""controllers.Comparisons.get(id:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comparisons""","""controllers.Comparisons.create()"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comparisons/$id<[^/]+>""","""controllers.Comparisons.update(id:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comparisons/$id<[^/]+>""","""controllers.Comparisons.delete(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comparisons/$comparisonId<[^/]+>/comments""","""controllers.Comparisons.getComments(comparisonId:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comparisons/$comparisonId<[^/]+>/votes""","""controllers.Comparisons.getVotes(comparisonId:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comparisons/$comparisonId<[^/]+>/itemAttributes""","""controllers.Comparisons.getItemAttributes(comparisonId:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comparisons/$comparisonId<[^/]+>/comments/$commentId<[^/]+>""","""controllers.Comparisons.getComment(comparisonId:String, commentId:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comparisons/$comparisonId<[^/]+>/votes/$voteId<[^/]+>""","""controllers.Comparisons.getVote(comparisonId:String, voteId:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comparisons/$comparisonId<[^/]+>/itemAttributes/$itemAttributeId<[^/]+>""","""controllers.Comparisons.getItemAttribute(comparisonId:String, itemAttributeId:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:6
case controllers_Application_index0(params) => {
   call(Param[String]("any", Right("none"))) { (any) =>
        invokeHandler(controllers.Application.index(any), HandlerDef(this, "controllers.Application", "index", Seq(classOf[String]),"GET", """ Home page""", Routes.prefix + """"""))
   }
}
        

// @LINE:7
case controllers_Application_upload1(params) => {
   call { 
        invokeHandler(controllers.Application.upload(), HandlerDef(this, "controllers.Application", "upload", Nil,"POST", """""", Routes.prefix + """upload"""))
   }
}
        

// @LINE:10
case controllers_Users_list2(params) => {
   call { 
        invokeHandler(controllers.Users.list(), HandlerDef(this, "controllers.Users", "list", Nil,"GET", """ API""", Routes.prefix + """api/users"""))
   }
}
        

// @LINE:11
case controllers_Users_settings3(params) => {
   call { 
        invokeHandler(controllers.Users.settings(), HandlerDef(this, "controllers.Users", "settings", Nil,"POST", """""", Routes.prefix + """api/users/settings"""))
   }
}
        

// @LINE:12
case controllers_Users_create4(params) => {
   call { 
        invokeHandler(controllers.Users.create(), HandlerDef(this, "controllers.Users", "create", Nil,"POST", """""", Routes.prefix + """api/users"""))
   }
}
        

// @LINE:13
case controllers_Users_follow5(params) => {
   call { 
        invokeHandler(controllers.Users.follow(), HandlerDef(this, "controllers.Users", "follow", Nil,"POST", """""", Routes.prefix + """api/users/follow"""))
   }
}
        

// @LINE:14
case controllers_Users_unfollow6(params) => {
   call { 
        invokeHandler(controllers.Users.unfollow(), HandlerDef(this, "controllers.Users", "unfollow", Nil,"POST", """""", Routes.prefix + """api/users/unfollow"""))
   }
}
        

// @LINE:15
case controllers_Users_get7(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Users.get(id), HandlerDef(this, "controllers.Users", "get", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/users/$id<[^/]+>"""))
   }
}
        

// @LINE:16
case controllers_Users_update8(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Users.update(id), HandlerDef(this, "controllers.Users", "update", Seq(classOf[String]),"PUT", """""", Routes.prefix + """api/users/$id<[^/]+>"""))
   }
}
        

// @LINE:18
case controllers_Items_list9(params) => {
   call { 
        invokeHandler(controllers.Items.list(), HandlerDef(this, "controllers.Items", "list", Nil,"GET", """""", Routes.prefix + """api/items"""))
   }
}
        

// @LINE:19
case controllers_Items_get10(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Items.get(id), HandlerDef(this, "controllers.Items", "get", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/items/$id<[^/]+>"""))
   }
}
        

// @LINE:20
case controllers_Items_create11(params) => {
   call { 
        invokeHandler(controllers.Items.create(), HandlerDef(this, "controllers.Items", "create", Nil,"POST", """""", Routes.prefix + """api/items"""))
   }
}
        

// @LINE:21
case controllers_Items_update12(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Items.update(id), HandlerDef(this, "controllers.Items", "update", Seq(classOf[String]),"PUT", """""", Routes.prefix + """api/items/$id<[^/]+>"""))
   }
}
        

// @LINE:22
case controllers_Items_delete13(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Items.delete(id), HandlerDef(this, "controllers.Items", "delete", Seq(classOf[String]),"DELETE", """""", Routes.prefix + """api/items/$id<[^/]+>"""))
   }
}
        

// @LINE:24
case controllers_ItemAttributes_list14(params) => {
   call { 
        invokeHandler(controllers.ItemAttributes.list(), HandlerDef(this, "controllers.ItemAttributes", "list", Nil,"GET", """""", Routes.prefix + """api/itemAttributes"""))
   }
}
        

// @LINE:25
case controllers_ItemAttributes_get15(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.ItemAttributes.get(id), HandlerDef(this, "controllers.ItemAttributes", "get", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/itemAttributes/$id<[^/]+>"""))
   }
}
        

// @LINE:26
case controllers_ItemAttributes_create16(params) => {
   call { 
        invokeHandler(controllers.ItemAttributes.create(), HandlerDef(this, "controllers.ItemAttributes", "create", Nil,"POST", """""", Routes.prefix + """api/itemAttributes"""))
   }
}
        

// @LINE:27
case controllers_ItemAttributes_update17(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.ItemAttributes.update(id), HandlerDef(this, "controllers.ItemAttributes", "update", Seq(classOf[String]),"PUT", """""", Routes.prefix + """api/itemAttributes/$id<[^/]+>"""))
   }
}
        

// @LINE:28
case controllers_ItemAttributes_delete18(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.ItemAttributes.delete(id), HandlerDef(this, "controllers.ItemAttributes", "delete", Seq(classOf[String]),"DELETE", """""", Routes.prefix + """api/itemAttributes/$id<[^/]+>"""))
   }
}
        

// @LINE:30
case controllers_Votes_list19(params) => {
   call { 
        invokeHandler(controllers.Votes.list(), HandlerDef(this, "controllers.Votes", "list", Nil,"GET", """""", Routes.prefix + """api/votes"""))
   }
}
        

// @LINE:31
case controllers_Votes_get20(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Votes.get(id), HandlerDef(this, "controllers.Votes", "get", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/votes/$id<[^/]+>"""))
   }
}
        

// @LINE:32
case controllers_Votes_create21(params) => {
   call { 
        invokeHandler(controllers.Votes.create(), HandlerDef(this, "controllers.Votes", "create", Nil,"POST", """""", Routes.prefix + """api/votes"""))
   }
}
        

// @LINE:33
case controllers_Votes_update22(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Votes.update(id), HandlerDef(this, "controllers.Votes", "update", Seq(classOf[String]),"PUT", """""", Routes.prefix + """api/votes/$id<[^/]+>"""))
   }
}
        

// @LINE:34
case controllers_Votes_delete23(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Votes.delete(id), HandlerDef(this, "controllers.Votes", "delete", Seq(classOf[String]),"DELETE", """""", Routes.prefix + """api/votes/$id<[^/]+>"""))
   }
}
        

// @LINE:36
case controllers_Comments_list24(params) => {
   call { 
        invokeHandler(controllers.Comments.list(), HandlerDef(this, "controllers.Comments", "list", Nil,"GET", """""", Routes.prefix + """api/comments"""))
   }
}
        

// @LINE:37
case controllers_Comments_get25(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Comments.get(id), HandlerDef(this, "controllers.Comments", "get", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/comments/$id<[^/]+>"""))
   }
}
        

// @LINE:38
case controllers_Comments_create26(params) => {
   call { 
        invokeHandler(controllers.Comments.create(), HandlerDef(this, "controllers.Comments", "create", Nil,"POST", """""", Routes.prefix + """api/comments"""))
   }
}
        

// @LINE:39
case controllers_Comments_update27(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Comments.update(id), HandlerDef(this, "controllers.Comments", "update", Seq(classOf[String]),"PUT", """""", Routes.prefix + """api/comments/$id<[^/]+>"""))
   }
}
        

// @LINE:40
case controllers_Comments_delete28(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Comments.delete(id), HandlerDef(this, "controllers.Comments", "delete", Seq(classOf[String]),"DELETE", """""", Routes.prefix + """api/comments/$id<[^/]+>"""))
   }
}
        

// @LINE:42
case controllers_Notifications_list29(params) => {
   call { 
        invokeHandler(controllers.Notifications.list(), HandlerDef(this, "controllers.Notifications", "list", Nil,"GET", """""", Routes.prefix + """api/notifications"""))
   }
}
        

// @LINE:43
case controllers_Notifications_get30(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Notifications.get(id), HandlerDef(this, "controllers.Notifications", "get", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/notifications/$id<[^/]+>"""))
   }
}
        

// @LINE:44
case controllers_Notifications_create31(params) => {
   call { 
        invokeHandler(controllers.Notifications.create(), HandlerDef(this, "controllers.Notifications", "create", Nil,"POST", """""", Routes.prefix + """api/notifications"""))
   }
}
        

// @LINE:45
case controllers_Notifications_update32(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Notifications.update(id), HandlerDef(this, "controllers.Notifications", "update", Seq(classOf[String]),"PUT", """""", Routes.prefix + """api/notifications/$id<[^/]+>"""))
   }
}
        

// @LINE:46
case controllers_Notifications_delete33(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Notifications.delete(id), HandlerDef(this, "controllers.Notifications", "delete", Seq(classOf[String]),"DELETE", """""", Routes.prefix + """api/notifications/$id<[^/]+>"""))
   }
}
        

// @LINE:48
case controllers_Comparisons_list34(params) => {
   call { 
        invokeHandler(controllers.Comparisons.list(), HandlerDef(this, "controllers.Comparisons", "list", Nil,"GET", """""", Routes.prefix + """api/comparisons"""))
   }
}
        

// @LINE:49
case controllers_Comparisons_get35(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Comparisons.get(id), HandlerDef(this, "controllers.Comparisons", "get", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/comparisons/$id<[^/]+>"""))
   }
}
        

// @LINE:50
case controllers_Comparisons_create36(params) => {
   call { 
        invokeHandler(controllers.Comparisons.create(), HandlerDef(this, "controllers.Comparisons", "create", Nil,"POST", """""", Routes.prefix + """api/comparisons"""))
   }
}
        

// @LINE:51
case controllers_Comparisons_update37(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Comparisons.update(id), HandlerDef(this, "controllers.Comparisons", "update", Seq(classOf[String]),"PUT", """""", Routes.prefix + """api/comparisons/$id<[^/]+>"""))
   }
}
        

// @LINE:52
case controllers_Comparisons_delete38(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.Comparisons.delete(id), HandlerDef(this, "controllers.Comparisons", "delete", Seq(classOf[String]),"DELETE", """""", Routes.prefix + """api/comparisons/$id<[^/]+>"""))
   }
}
        

// @LINE:55
case controllers_Comparisons_getComments39(params) => {
   call(params.fromPath[String]("comparisonId", None)) { (comparisonId) =>
        invokeHandler(controllers.Comparisons.getComments(comparisonId), HandlerDef(this, "controllers.Comparisons", "getComments", Seq(classOf[String]),"GET", """ Relation of Comparison""", Routes.prefix + """api/comparisons/$comparisonId<[^/]+>/comments"""))
   }
}
        

// @LINE:56
case controllers_Comparisons_getVotes40(params) => {
   call(params.fromPath[String]("comparisonId", None)) { (comparisonId) =>
        invokeHandler(controllers.Comparisons.getVotes(comparisonId), HandlerDef(this, "controllers.Comparisons", "getVotes", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/comparisons/$comparisonId<[^/]+>/votes"""))
   }
}
        

// @LINE:57
case controllers_Comparisons_getItemAttributes41(params) => {
   call(params.fromPath[String]("comparisonId", None)) { (comparisonId) =>
        invokeHandler(controllers.Comparisons.getItemAttributes(comparisonId), HandlerDef(this, "controllers.Comparisons", "getItemAttributes", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/comparisons/$comparisonId<[^/]+>/itemAttributes"""))
   }
}
        

// @LINE:58
case controllers_Comparisons_getComment42(params) => {
   call(params.fromPath[String]("comparisonId", None), params.fromPath[String]("commentId", None)) { (comparisonId, commentId) =>
        invokeHandler(controllers.Comparisons.getComment(comparisonId, commentId), HandlerDef(this, "controllers.Comparisons", "getComment", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """api/comparisons/$comparisonId<[^/]+>/comments/$commentId<[^/]+>"""))
   }
}
        

// @LINE:59
case controllers_Comparisons_getVote43(params) => {
   call(params.fromPath[String]("comparisonId", None), params.fromPath[String]("voteId", None)) { (comparisonId, voteId) =>
        invokeHandler(controllers.Comparisons.getVote(comparisonId, voteId), HandlerDef(this, "controllers.Comparisons", "getVote", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """api/comparisons/$comparisonId<[^/]+>/votes/$voteId<[^/]+>"""))
   }
}
        

// @LINE:60
case controllers_Comparisons_getItemAttribute44(params) => {
   call(params.fromPath[String]("comparisonId", None), params.fromPath[String]("itemAttributeId", None)) { (comparisonId, itemAttributeId) =>
        invokeHandler(controllers.Comparisons.getItemAttribute(comparisonId, itemAttributeId), HandlerDef(this, "controllers.Comparisons", "getItemAttribute", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """api/comparisons/$comparisonId<[^/]+>/itemAttributes/$itemAttributeId<[^/]+>"""))
   }
}
        

// @LINE:64
case controllers_Assets_at45(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
   }
}
        
}

}
     