'use strict';

/* Controllers */

var octopusControllers = angular.module('octopusControllers', []);

octopusControllers.controller('ListModelCtrl', ['$scope', 'OctopusModel',
  function($scope, OctopusModel) {
    
	$scope.octopusModels = OctopusModel.query();
    
    $scope.showModel = function(modelId) {
    	$scope.octopusModel = OctopusModel.get({id: modelId}, function(octopusModel) {
    		   return octopusModel;
    	});
    	$("li.model-item").removeClass("active");
    	$($("a[data-id=" + modelId + ']').closest("li.model-item")).addClass("active");
    	$("#run_model_btn").removeAttr("disabled");
    }
  }]);

octopusControllers.controller('RunModelCtrl', ['$scope', '$routeParams', 'OctopusModel',
  function($scope, $routeParams, OctopusModel) {
		
  }]);

  