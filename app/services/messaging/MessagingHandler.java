package services.messaging;

import configs.Constants;

import java.util.List;

/**
 * Created by dtn29 on 4/22/14.
 */
public interface MessagingHandler {

    public void send(String contentKey, String fromAddress, List<String> toAddresses) throws MessagingException;

    public void send(Constants.AlertLevel alertLevel, String fromAddress, List<String> toAddresses) throws MessagingException;
}
