package services.messaging.impl;

import com.typesafe.plugin.MailerAPI;
import com.typesafe.plugin.MailerPlugin;
import configs.Constants;
import services.messaging.MessagingException;
import services.messaging.MessagingHandler;

import java.util.List;

/**
 * Created by dtn29 on 4/13/14.
 */
public class EmailMessaging implements MessagingHandler{

    @Override
    public void send(String contentKey, String fromAddress, List<String> toAddresses) throws MessagingException {

    }

    @Override
    public void send(Constants.AlertLevel alertLevel, String fromAddress, List<String> toAddresses) throws MessagingException {
        if (fromAddress == null) throw new MessagingException("From email cannot be null");
        if (toAddresses == null) throw new MessagingException("To emails cannot be null");
        MailerAPI mail = play.Play.application().plugin(MailerPlugin.class).email();
        mail.addFrom(fromAddress);
        for (String email : toAddresses) {
            mail.addRecipient(email);
            if (alertLevel.equals(Constants.AlertLevel.RED)) {
                mail.setSubject("EMERGENCY - RED ALERT");
                mail.sendHtml("SERVER IS DOWN. FIX IT NOW");
            } else if (alertLevel.equals(Constants.AlertLevel.YELLOW)) {
                mail.setSubject("Something went wrong");
                mail.sendHtml("There is error occur in your site. You should check it as soon as possible");
            } else {
                mail.setSubject("Bad request happen");
                mail.sendHtml("There is bad request occur in your site. Maybe someone try to hack it. Take a look if you have time");
            }
        }

    }
}
