package services.messaging.impl;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.SmsFactory;
import configs.Constants;
import play.Logger;
import services.messaging.MessagingException;
import services.messaging.MessagingHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dtn29 on 4/13/14.
 */
public class PhoneMessaging implements MessagingHandler {


    @Override
    public void send(String contentKey, String fromAddress, List<String> toAddresses) throws MessagingException {

    }

    @Override
    public void send(Constants.AlertLevel alertLevel, String fromAddress, List<String> toAddresses) throws MessagingException {
        if (alertLevel.equals(Constants.AlertLevel.RED) == false) return;
        if (fromAddress == null) throw new MessagingException("From phone number cannot be null");
        if (toAddresses == null) throw new MessagingException("To phone number cannot be null");
        try {
            TwilioRestClient client = new TwilioRestClient(play.Play.application().configuration().getString(Constants.TWILIO_SID),
                    play.Play.application().configuration().getString(Constants.TWILIO_AUTH_TOKEN));
            Map<String, String> params = new HashMap<String, String>();
            params.put("Body", "EMERGENCY - SERVER DOWN");
            params.put("From", fromAddress);
            for (String phoneNumber : toAddresses) {
                params.put("To", phoneNumber);
                SmsFactory messageFactory = client.getAccount().getSmsFactory();
                messageFactory.create(params);
            }
        } catch (TwilioRestException e) {
            Logger.error("Send sms exception",e);
        }

    }
}
