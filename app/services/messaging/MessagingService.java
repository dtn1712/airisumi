package services.messaging;

import configs.Constants;
import services.messaging.impl.EmailMessaging;
import services.messaging.impl.PhoneMessaging;

import java.util.List;

/**
 * Created by dtn29 on 4/13/14.
 */
public class MessagingService {

    private Constants.AlertLevel alertLevel;
    private String contentKey;
    private String fromEmail;
    private String fromNumber;
    private List<String> toEmails;
    private List<String> toNumbers;

    public void execute() throws MessagingException {
        MessagingHandler emailMessaging = new EmailMessaging();
        MessagingHandler phoneMessaging = new PhoneMessaging();
        if (alertLevel == null) {
            emailMessaging.send(this.contentKey,this.fromEmail, this.toEmails);
        } else {
            if (alertLevel.equals(Constants.AlertLevel.RED)) {
                emailMessaging.send(this.alertLevel,this.fromEmail,this.toEmails);
                phoneMessaging.send(this.alertLevel,this.fromNumber,this.toNumbers);
            } else {
                emailMessaging.send(this.alertLevel,this.fromEmail,this.toEmails);
            }
        }
    }

    public String getContentKey() {
        return contentKey;
    }

    public void setContentKey(String contentKey) {
        this.contentKey = contentKey;
    }

    public Constants.AlertLevel getAlertLevel() {
        return alertLevel;
    }

    public void setAlertLevel(Constants.AlertLevel alertLevel) {
        this.alertLevel = alertLevel;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(String fromNumber) {
        this.fromNumber = fromNumber;
    }

    public List<String> getToEmails() {
        return toEmails;
    }

    public void setToEmails(List<String> toEmails) {
        this.toEmails = toEmails;
    }

    public List<String> getToNumbers() {
        return toNumbers;
    }

    public void setToNumbers(List<String> toNumbers) {
        this.toNumbers = toNumbers;
    }
}
