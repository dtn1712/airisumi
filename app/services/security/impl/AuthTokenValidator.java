package services.security.impl;

import configs.Constants;
import models.AuthToken;
import play.mvc.Http;
import services.security.*;
import services.security.SecurityException;

/**
 *
 * Created by dtn29 on 4/13/14.
 */
public class AuthTokenValidator implements RequestValidator {

    @Override
    public void validate(Http.Context ctx) throws services.security.SecurityException {
        AuthToken authToken = getHeaderAuthToken(ctx);
        if (authToken == null) {
            throw new SecurityException(401,"Invalid Token",ctx.request());
        } else {
            ctx.args.put(Constants.CURRENT_LOGIN_USER, authToken.getUserId());
        }
    }

    public AuthToken getHeaderAuthToken(Http.Context ctx) {
        AuthToken authToken = null;
        String[] authTokenHeaderValues = ctx.request().headers().get(Constants.AUTH_TOKEN_HEADER);
        if ((authTokenHeaderValues != null) && (authTokenHeaderValues.length == 1) && (authTokenHeaderValues[0] != null)) {
            authToken = AuthToken.getOneByUniqueField(Constants.AUTH_TOKEN,authTokenHeaderValues[0]);
        }
        return authToken;
    }
}
