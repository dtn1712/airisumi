package helpers;

import com.fasterxml.jackson.databind.JsonNode;
import models.S3File;
import play.libs.Json;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GlobalHelper {
	
	public static S3File uploadFileToS3(File file, String fileName) {
		S3File s3File = new S3File();
		s3File.name = fileName;
		s3File.file = file;
		s3File.save();
		return s3File;    
    }


    public static Object setValueIfItemNull(Object item,Object value) {
        if (item == null) {
            return value;
        }
        return item;
    }

    public static Map<String,JsonNode> getLimitFieldsReturn(Map<String,Object> allFields, List<String> limitFields) {
        Map<String,JsonNode> data = new HashMap<String,JsonNode>();
        if (limitFields != null) {
            for (String field : limitFields) {
                if (allFields.containsKey(field)) {
                    Object value = allFields.get(field);
                    data.put(field,Json.toJson(value));
                }
            }
        } else {
            for (Map.Entry<String,Object> entry : allFields.entrySet()) {
                data.put(entry.getKey(),Json.toJson(entry.getValue()));
            }
        }
        return data;
    }

    public static Object chooseMessageIfObjectNull(Object obj, String message) {
        if (obj == null) {
            return message;
        }
        return obj;
    }
}
