package plugins;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import configs.AWSConfig;
import play.Application;
import play.Logger;
import play.Plugin;

public class S3Plugin extends Plugin {
   
    private final Application application;

    public static AmazonS3 amazonS3;

    public static String s3Bucket;

    public S3Plugin(Application application) {
        this.application = application;
    }

    @Override
    public void onStart() {
        String accessKey = application.configuration().getString(AWSConfig.AWS_ACCESS_KEY);
        String secretKey = application.configuration().getString(AWSConfig.AWS_SECRET_KEY);
        s3Bucket = application.configuration().getString(AWSConfig.AWS_S3_BUCKET);
        if ((accessKey != null) && (secretKey != null)) {
            AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
            amazonS3 = new AmazonS3Client(awsCredentials);
            try {
            	amazonS3.createBucket(s3Bucket);
            } catch (AmazonServiceException e) {
            	Logger.info("Bucket is already created");
            }
            Logger.info("Using S3 Bucket: " + s3Bucket);
        }
    }

    @Override
    public boolean enabled() {
        return (application.configuration().keys().contains(AWSConfig.AWS_ACCESS_KEY) &&
                application.configuration().keys().contains(AWSConfig.AWS_SECRET_KEY) &&
                application.configuration().keys().contains(AWSConfig.AWS_S3_BUCKET));
    }

}