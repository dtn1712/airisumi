import configs.Constants;
import models.*;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.Play;
import play.api.mvc.EssentialFilter;
import play.filters.gzip.GzipFilter;
import play.libs.F;
import play.mvc.Http;
import play.mvc.SimpleResult;
import services.messaging.MessagingService;

import java.util.List;

/**
 * Created by dtn29 on 4/12/14.
 */
public class Global extends GlobalSettings {

    public <T extends EssentialFilter> Class<T>[] filters() {
        return new Class[]{GzipFilter.class};
    }

    @Override
    public void onStart(Application app) {
        MongoDbModel[] models = new MongoDbModel[] {
            new AuthToken(), new Comment(), new Comparison(), new IPRequest(),
            new Item(), new ItemAttribute(), new Notification(), new Notification(),
            new Setting(), new User(), new Vote()
        };
        for (MongoDbModel model : models) {
            model.buildIndex();
        }
    }

    /**
     * When the server down, alert admin immediately (RED ALERT)
     * @param app
     */
    @Override
    public void onStop(Application app) {
        if (Play.isProd()) {
            String fromEmail = Play.application().configuration().getString(Constants.SMTP_USER);
            String fromNumber = Play.application().configuration().getString(Constants.TWILIO_PHONE_NUMBER);
            List<String> toEmails = Play.application().configuration().getStringList(Constants.ALERT_TO_EMAIL_KEY);
            List<String> toNumbers = Play.application().configuration().getStringList(Constants.ALERT_TO_PHONE_KEY);
            MessagingService messagingService = new MessagingService();
            messagingService.setAlertLevel(Constants.AlertLevel.RED);
            messagingService.setFromEmail(fromEmail);
            messagingService.setToEmails(toEmails);
            messagingService.setFromNumber(fromNumber);
            messagingService.setToNumbers(toNumbers);
            try {
                messagingService.execute();
            } catch (Exception e) {
                Logger.error("Sending alert messages exception",e);
            }
        }
    }

    /**
     * If exception happen, alert admin immediately (YELLOW ALERT)
     * @param request
     * @param t
     * @return
     */
    @Override
    public F.Promise<SimpleResult> onError(Http.RequestHeader request, Throwable t) {
        if (Play.isProd()) {
            String fromEmail = Play.application().configuration().getString(Constants.SMTP_USER);
            String fromNumber = Play.application().configuration().getString(Constants.TWILIO_PHONE_NUMBER);
            List<String> toEmails = Play.application().configuration().getStringList(Constants.ALERT_TO_EMAIL_KEY);
            List<String> toNumbers = Play.application().configuration().getStringList(Constants.ALERT_TO_PHONE_KEY);
            MessagingService messagingService = new MessagingService();
            messagingService.setAlertLevel(Constants.AlertLevel.YELLOW);
            messagingService.setFromEmail(fromEmail);
            messagingService.setToEmails(toEmails);
            messagingService.setFromNumber(fromNumber);
            messagingService.setToNumbers(toNumbers);
            try {
                messagingService.execute();
            } catch (Exception e) {
                Logger.error("Sending alert messages exception",e);
            }
        }
        return null;
    }

    /**
     * If bad request happen, notify admin (GREEN ALERT)
     * @param request
     * @param error
     * @return
     */
    @Override
    public F.Promise<SimpleResult> onBadRequest(Http.RequestHeader request, String error) {
        if (Play.isProd()) {
            String fromEmail = Play.application().configuration().getString(Constants.SMTP_USER);
            String fromNumber = Play.application().configuration().getString(Constants.TWILIO_PHONE_NUMBER);
            List<String> toEmails = Play.application().configuration().getStringList(Constants.ALERT_TO_EMAIL_KEY);
            List<String> toNumbers = Play.application().configuration().getStringList(Constants.ALERT_TO_PHONE_KEY);
            MessagingService messagingService = new MessagingService();
            messagingService.setAlertLevel(Constants.AlertLevel.GREEN);
            messagingService.setFromEmail(fromEmail);
            messagingService.setToEmails(toEmails);
            messagingService.setFromNumber(fromNumber);
            messagingService.setToNumbers(toNumbers);
            try {
                messagingService.execute();
            } catch (Exception e) {
                Logger.error("Sending alert messages exception",e);
            }
        }
        return null;
    }

}
