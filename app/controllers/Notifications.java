package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import configs.Constants;
import helpers.GlobalHelper;
import helpers.HttpHelper;
import helpers.MongoDbHelper;
import models.Notification;
import models.User;
import play.Logger;
import play.libs.Json;
import play.mvc.*;
import services.security.SecurityService;

import java.util.List;
import java.util.Map;


@With(SecurityService.class)
public class Notifications extends Controller {

    public static Result list() {
        try {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            Map<String,String> sortCriteria = HttpHelper.buildSortCriteriaFromRequest(request());
            Map<String,String> queryCriteria = HttpHelper.buildQueryCriteriaFromRequest(request());
            Map<String,Integer> pagination = HttpHelper.buildPaginationFromRequest(request());
            List<ObjectNode> notifications = MongoDbHelper.queryMongoDb(Notification.getCollection(), queryCriteria,
                    sortCriteria.get(Constants.SORT_FIELD), sortCriteria.get(Constants.SORT_TYPE),
                    pagination.get(Constants.PAGE_NUMBER), pagination.get(Constants.NUM_RESULTS),
                    limitFields, expandFields);
            return ok(HttpHelper.buildDataResponse(200,notifications));
        } catch (Exception e) {
            Logger.error("Cannot list the notifications",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Fail to execute the query",e.getMessage()));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result create() {
        JsonNode json = request().body().asJson();
        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
        try {
            if (HttpHelper.isJsonRequestContainValidFields(new Notification(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The json fields is invalid"));
            Notification newNotification = Notification.mapJsonToObject(json,null);
            User notifyFromUser = User.getById(newNotification.getNotifyFromUserId());
            if (notifyFromUser == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The notify from user id " + notifyFromUser.getId() + " is invalid"));
            Notification.create(newNotification);
            return Results.created(HttpHelper.buildDataResponse(201, newNotification));
        } catch (Exception e) {
            Logger.error("Create notification exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot create notification",e.getMessage()));
        }
    }

    public static Result get(String id) {
        Notification notification = Notification.getById(id);
        ObjectNode result = Json.newObject();
        if (notification == null) {
            return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the notification with id " + id));
        } else {
            List<String> limitFields = HttpHelper.buildLimitFieldsFromRequest(request());
            List<String> expandFields = HttpHelper.buildExpandFieldsFromRequest(request());
            if (expandFields != null) notification.expandRelatedFields(expandFields);
            if (limitFields != null) {
                notification.buildFields();
                Map<String,Object> allFields = notification.getFields();
                Map<String,JsonNode> data = GlobalHelper.getLimitFieldsReturn(allFields, limitFields);
                result.putAll(data);
                return ok(HttpHelper.buildDataResponse(200,result));
            }
            return ok(HttpHelper.buildDataResponse(200, notification));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result update(String id) {
        JsonNode json = request().body().asJson();
        if (json == null ) return badRequest(HttpHelper.buildErrorFormatResponse(400,"The request cannot be parsed to json or is null"));
        try {
            Notification oldNotification = Notification.getById(id);
            if (oldNotification == null) return notFound(HttpHelper.buildErrorFormatResponse(404, "Cannot find notification with id " + id));
            if (HttpHelper.isJsonRequestContainValidFields(new Notification(),json) == false) return badRequest(HttpHelper.buildErrorFormatResponse(400, "The json fields is invalid"));
            Notification newNotification = Notification.mapJsonToObject(json, oldNotification);
            Notification updatedNotification = Notification.update(newNotification);
            if (updatedNotification == null) throw new Exception("Fail to update the notification. Please try again");
            return ok(HttpHelper.buildDataResponse(200,updatedNotification));
        } catch (Exception e) {
            Logger.error("Update notification exception",e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot update notification",e.getMessage()));
        }
    }

    public static Result delete(String id) {
        try {
            Notification notification = Notification.getById(id);
            if (notification == null) {
                return notFound(HttpHelper.buildErrorFormatResponse(404,"Cannot find the notification to delete"));
            }
            Notification.delete(id);
            return ok(HttpHelper.buildDataResponse(200,"Delete notification successfully"));
        } catch (Exception e) {
            Logger.error("Delete notification exception", e);
            return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot delete notification",e.getMessage()));
        }
    }
}
