package controllers;

import configs.Constants;
import helpers.GlobalHelper;
import helpers.HttpHelper;
import models.S3File;
import play.Logger;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import play.mvc.With;
import services.security.SecurityService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@With(SecurityService.class)
public class Application extends Controller {

	public static Result index(String any) {
        return ok(views.html.index.render());
	}

	public static Result upload() {
		List<FilePart> files = request().body().asMultipartFormData().getFiles();
		List<S3File> s3Files = new ArrayList<S3File>();
		try {
			for (FilePart file : files) {
				S3File s3File = GlobalHelper.uploadFileToS3(file.getFile(), file.getFilename());
				s3Files.add(s3File);
			}
			return ok(HttpHelper.buildDataResponse(200,s3Files));
		} catch (Exception e) {
			Logger.error("Upload File Exception",e);
		}
		return internalServerError(HttpHelper.buildErrorFormatResponse(500,"Cannot upload the file"));
			
	}
  
}
