package configs;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * Created by dtn29 on 4/12/14.
 */
public class Constants {

    public static final String ERROR_FIELD_KEY = "error";

    public static final String AMAZON_S3_URL = "https://s3-us-west-2.amazonaws.com";

    public static final String CURRENT_LOGIN_USER = "loginUserId";

    public static final String ASCENDING_SORT = "asc";
    public static final String DESCENDING_SORT = "desc";

    public static final int DEFAULT_TOTAL_RETURN = 20;
    public static final String NUM_RESULTS = "numResults";
    public static final String PAGE_NUMBER = "pageNum";

    public static final String AUTH_TOKEN_HEADER = "X-AUTH-TOKEN";
    public static final String AUTH_TOKEN = "authToken";

    public static final String USER_AGENT_UNDEFINED = "undefined";

    public static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

    public static final String FACEBOOK_GRAPH_URL = "http://graph.facebook.com/";
    public static final String FACEBOOK_GRAPH_DEBUG_ACCESS_TOKEN_URL = "https://graph.facebook.com/debug_token?input_token=%s&access_token=%s";

    public static final String LIMIT_FIELDS = "fields";
    public static final String EXPAND_FIELDS = "expand";

    public static final String SORT_FIELD = "sortField";
    public static final String SORT_TYPE = "sortType";
    public static final String SORT_KEYWORD = "sort";

    public static final String PRICE_FIELD = "price";

    public static final String HTTP_STATUS = "status";
    public static final String HTTP_CODE = "http_code";
    public static final String HTTP_NORMAL_MESSAGE = "message";
    public static final String HTTP_ERROR_MESSAGE = "error_message";
    public static final String HTTP_EXCEPTION_MESSAGE = "exception_message";
    public static final String HTTP_RESULTS = "results";

    public static final String NEW_STATUS = "new";
    public static final String OLD_STATUS = "old";

    public static final String SMTP_USER = "smtp.user";

    public static final String ALERT_TO_EMAIL_KEY = "alert.email.send.to";
    public static final String ALERT_TO_PHONE_KEY = "alert.phone.send.to";

    public static final String TWILIO_SID = "twilio.sid";
    public static final String TWILIO_AUTH_TOKEN = "twilio.token";
    public static final String TWILIO_PHONE_NUMBER = "twilio.phone.number";

    public static final String FIELD_SEPARATOR = "__";

    public static final Map<String,String> NUMERIC_COMPARE_FIELD_KEY = ImmutableMap.<String, String>builder()
            .put(PRICE_FIELD, "")
            .build();

    public static final Map<String,String> COMPARISON_QUERY_OPERATORS = ImmutableMap.<String,String>builder()
            .put("gt","$gt")
            .put("gte","$gte")
            .put("lt","$lt")
            .put("lte","$lte")
            .build();

    public static enum AlertLevel {
        GREEN, YELLOW, RED
    }

}
