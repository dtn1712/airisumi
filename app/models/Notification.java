package models;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.BasicDBObject;
import configs.Constants;
import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.JacksonDBCollection;
import net.vz.mongodb.jackson.ObjectId;
import net.vz.mongodb.jackson.WriteResult;
import play.Logger;
import play.modules.mongodb.jackson.MongoDB;

public class Notification extends MongoDbModel{
	
	@Id
	@ObjectId
	private String id;
	
	private String notifyFromUserId;
	private Map<String,String> notifyToUser;
	private Date createTime;
	private String content;
	private String status;

    private Map<String,Object> expandFields;

    @JsonIgnore
    private Map<String,Object> fields;

    public Notification() {
        this.createTime = new Date();
        this.expandFields = new HashMap<String,Object>();
        this.fields = new HashMap<String,Object>();
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNotifyFromUserId() {
		return notifyFromUserId;
	}

	public void setNotifyFromUserId(String notifyFromUserId) {
		this.notifyFromUserId = notifyFromUserId;
	}

	public Map<String, String> getNotifyToUser() {
		return notifyToUser;
	}

	public void setNotifyToUser(Map<String, String> notifyToUser) {
		this.notifyToUser = notifyToUser;
	}

    public void addNotifyToUser(User user) {
        if (user == null) return;
        if (this.notifyToUser == null) {
            this.notifyToUser = new HashMap<String,String>();
        }
        this.notifyToUser.put(user.getId(), user.getFirstName() + " " + user.getLastName());
    }

    public void removeNotifyToUser(User user) {
        if (user == null) return;
        if (this.notifyToUser == null) {
            this.notifyToUser = new HashMap<String,String>();
            return;
        }
        this.notifyToUser.remove(user.getId());
    }

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

    public Map<String, Object> getExpandFields() {
        return expandFields;
    }

    public void setExpandFields(Map<String, Object> expandFields) {
        this.expandFields = expandFields;
    }

    @Override
    public Map<String, Object> getFields() { return fields; }

    private static JacksonDBCollection<Notification, String> collection = MongoDB.getCollection("notification", Notification.class, String.class);

    public static JacksonDBCollection<Notification, String> getCollection() {
        return collection;
    }

    public static Notification getById(String id) {
        try {
            return collection.findOneById(id);
        } catch (Exception e) {
            Logger.error("Fail to get notification by id " + id, e);
        }
        return null;
    }

    public static void create(Notification notification) {
        try {
            if (notification == null) throw new NullPointerException();
            WriteResult<Notification,String> result = collection.insert(notification);
            notification.setId(result.getSavedId());
        } catch (Exception e) {
            Logger.error("Fail to create new notification",e);
        }
    }

    public static Notification update(Notification notification) {
        try {
            if (notification == null) throw new NullPointerException();
            WriteResult<Notification,String> result = collection.save(notification);
            return result.getSavedObject();
        } catch (Exception e) {
            Logger.error("Fail to update notification " + notification.getId(),e);
        }
        return null;
    }

    public static void delete(String id) {
        try {
            Notification notification = collection.findOneById(id);
            if (notification != null) {
                collection.remove(notification);
            }
        } catch (Exception e) {
            Logger.error("Fail to delete notification " + id, e);
        }

    }

    public static void bulkDelete(Map<String,String> queryCriteria) {
        if (queryCriteria == null || queryCriteria.size() == 0) return;
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
        for (Map.Entry<String, String> entry : queryCriteria.entrySet()) {
            obj.add(new BasicDBObject(entry.getKey(),new BasicDBObject("$regex",entry.getValue()).append("$options", "i")));
        }
        query.put("$and", obj);
        collection.remove(query);
    }

    public static Notification mapJsonToObject(JsonNode json, Notification notification) {
        try {
            if (json == null) throw new NullPointerException();
            if (notification == null) {
                notification = new Notification();
                notification.setStatus(Constants.NEW_STATUS);
                notification.setContent(json.findPath("content").asText());
                notification.setNotifyFromUserId(json.findPath("notifyFromUserId").asText());
                notification.setNotifyToUser(new HashMap<String, String>());
                JsonNode notifyToUser = json.findPath("notifyToUser");
                if (notifyToUser.isArray()) {
                    for (JsonNode userIdNode : notifyToUser) {
                        notification.addNotifyToUser(User.getById(userIdNode.asText()));
                    }
                }
            } else {
                if (json.findPath("content").isTextual())  notification.setContent(json.findPath("content").asText());
                if (json.findPath("notifyFromUserId").isTextual()) notification.setNotifyFromUserId(json.findPath("notifyFromUserId").asText());
                if (json.findPath("status").isTextual()) notification.setStatus(json.findPath("status").asText());
                JsonNode removeNotifyUser = json.findPath("removeNotifyUser");
                JsonNode addNotifyUser = json.findPath("addNotifyUser");
                if (removeNotifyUser.isArray()) {
                    for (JsonNode userIdNode : removeNotifyUser) {
                        notification.removeNotifyToUser(User.getById(userIdNode.asText()));
                    }
                }
                if (addNotifyUser.isArray()) {
                    for (JsonNode userIdNode : addNotifyUser) {
                        notification.addNotifyToUser(User.getById(userIdNode.asText()));
                    }
                }
            }
            return notification;
        } catch (Exception e) {
            Logger.error("Fail to map json data to notification object",e);
        }
        return null;
    }

    @Override
    public void expandRelatedFields(List<String> expandFields){
        if (expandFields == null) return;
        if (this.expandFields == null) {
            this.expandFields = new HashMap<String,Object>();
        }
        for (String field : expandFields) {
            if (field.equals("notifyFromUser")) {
                User user = User.getById(notifyFromUserId);
                if (user == null) {
                    this.expandFields.put("notifyFromUser","User Id is invalid");
                } else {
                    user.applySecurity();
                    this.expandFields.put("notifyFromUser",user.getFields());
                }
            } else if (field.equals("notifyToUser")) {
                List<Object> data = new ArrayList<Object>();
                for (Map.Entry<String,String> entry : notifyToUser.entrySet())  {
                    User user = User.getById(entry.getKey());
                    if (user == null) {
                        data.add("User Id " + entry.getKey() + " is invalid");
                    } else {
                        user.applySecurity();
                        data.add(user);
                    }
                }
                this.expandFields.put("notifyToUser",data);
            }
        }

    }

    @Override
    public void buildFields() {
        if (fields == null) {
            fields = new HashMap<String,Object>();
        }
        if (fields.containsKey("id") == false) {
            fields.put("id",id);
            fields.put("notifyFromUserId",notifyFromUserId);
            fields.put("createTime",createTime);
            fields.put("content",content);
            fields.put("status",status);
            fields.put("notifyToUser",notifyToUser);
            fields.put("expand",expandFields);
            fields.put("removeNotifyUser",null);
            fields.put("addNotifyUser",null);
        }
    }

    @Override
    public void buildIndex() {

    }

}
