//package models;
//
//import java.util.*;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.mongodb.BasicDBObject;
//import net.vz.mongodb.jackson.Id;
//import net.vz.mongodb.jackson.JacksonDBCollection;
//import net.vz.mongodb.jackson.ObjectId;
//import net.vz.mongodb.jackson.WriteResult;
//import play.Logger;
//import play.modules.mongodb.jackson.MongoDB;
//
//public class Photo extends MongoDbModel {
//
//	@Id
//	@ObjectId
//	private String id;
//
//	private String name;
//	private String linkPhoto;
//	private String createUserId;
//	private Date createTime;
//	private long sizeByte;
//
//    private Map<String,Object> expandFields;
//
//    @JsonIgnore
//    private Map<String,Object> fields;
//
//    public Photo() {
//        this.createTime = new Date();
//        this.expandFields = new HashMap<String,Object>();
//        this.fields = new HashMap<String,Object>();
//    }
//
//	public String getId() {
//		return id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getLinkPhoto() {
//		return linkPhoto;
//	}
//
//	public void setLinkPhoto(String linkPhoto) {
//		this.linkPhoto = linkPhoto;
//	}
//
//    public String getCreateUserId() {
//        return createUserId;
//    }
//
//    public void setCreateUserId(String createUserId) {
//        this.createUserId = createUserId;
//    }
//
//    public Date getCreateTime() {
//		return createTime;
//	}
//
//	public void setCreateTime(Date createTime) {
//		this.createTime = createTime;
//	}
//
//	public long getSizeByte() {
//		return sizeByte;
//	}
//
//	public void setSizeByte(long sizeByte) {
//		this.sizeByte = sizeByte;
//	}
//
//    public Map<String, Object> getExpandFields() {
//        return expandFields;
//    }
//
//    public void setExpandFields(Map<String, Object> expandFields) {
//        this.expandFields = expandFields;
//    }
//
//    @Override
//    public Map<String, Object> getFields() { return fields; }
//
//    private static JacksonDBCollection<Photo, String> collection = MongoDB.getCollection("photo", Photo.class, String.class);
//
//    public static JacksonDBCollection<Photo, String> getCollection() {
//        return collection;
//    }
//
//    public static Photo getById(String id) {
//        try {
//            return collection.findOneById(id);
//        } catch (Exception e) {
//            Logger.error("Fail to get photo by id " + id, e);
//        }
//        return null;
//    }
//
//    public static void create(Photo photo) {
//        try {
//            if (photo == null) throw new NullPointerException();
//            WriteResult<Photo,String> result = collection.insert(photo);
//            photo.setId(result.getSavedId());
//        } catch (Exception e) {
//            Logger.error("Fail to create new photo",e);
//        }
//    }
//
//    public static Photo update(Photo photo) {
//        try {
//            if (photo == null) throw new NullPointerException();
//            WriteResult<Photo,String> result = collection.save(photo);
//            return result.getSavedObject();
//        } catch (Exception e) {
//            Logger.error("Fail to update photo " + photo.getId(),e);
//        }
//        return null;
//    }
//
//    public static void delete(String id) {
//        try {
//            Photo photo = collection.findOneById(id);
//            if (photo != null) {
//                collection.remove(photo);
//            }
//        } catch (Exception e) {
//            Logger.error("Fail to delete photo " + id,e);
//        }
//    }
//
//    public static void bulkDelete(Map<String,String> queryCriteria) {
//        if (queryCriteria == null || queryCriteria.size() == 0) return;
//        BasicDBObject query = new BasicDBObject();
//        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
//        for (Map.Entry<String, String> entry : queryCriteria.entrySet()) {
//            obj.add(new BasicDBObject(entry.getKey(),new BasicDBObject("$regex",entry.getValue()).append("$options", "i")));
//        }
//        query.put("$and", obj);
//        collection.remove(query);
//    }
//
//    public static Photo mapJsonToObject(JsonNode json, Photo photo) {
//        try {
//            if (json == null) throw new NullPointerException();
//            if (photo == null) {
//                photo = new Photo();
//                photo.setName(json.findPath("name").asText());
//                photo.setLinkPhoto(json.findPath("linkPhoto").asText());
//                photo.setCreateUserId(json.findPath("createUserId").asText());
//                photo.setSizeByte(json.findPath("sizeByte").asLong());
//            } else {
//                if (json.findPath("name").isTextual())  photo.setName(json.findPath("name").asText());
//                if (json.findPath("linkPhoto").isTextual()) photo.setLinkPhoto(json.findPath("linkPhoto").asText());
//                if (json.findPath("sizeByte").isLong()) photo.setSizeByte(json.findPath("sizeByte").asLong());
//                if (json.findPath("createUserId").isTextual()) photo.setCreateUserId(json.findPath("createUserId").asText());
//            }
//            return photo;
//        } catch (Exception e) {
//            Logger.error("Fail to map json data to user object",e);
//        }
//        return null;
//    }
//
//    @Override
//    public void expandRelatedFields(List<String> expandFields){
//        if (expandFields == null) return;
//        if (this.expandFields == null) {
//            this.expandFields = new HashMap<String,Object>();
//        }
//        for (String field : expandFields) {
//            if (field.equals("createUser")) {
//                User user = User.getById(createUserId);
//                if (user == null) {
//                    this.expandFields.put("createUser","User Id " + createUserId + " is invalid");
//                } else {
//                    user.applySecurity();
//                    this.expandFields.put("createUser",user.getFields());
//                }
//            }
//        }
//
//    }
//
//    @Override
//    public void buildFields() {
//        if (fields == null) {
//            fields = new HashMap<String,Object>();
//        }
//        if (fields.containsKey("id") == false) {
//            fields.put("id",id);
//            fields.put("name",name);
//            fields.put("linkPhoto",linkPhoto);
//            fields.put("createUserId",createUserId);
//            fields.put("createTime",createTime);
//            fields.put("sizeByte",sizeByte);
//            fields.put("expand",expandFields);
//        }
//    }
//
//    @Override
//    public void buildIndex() {
//
//    }
//
//}
