package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.BasicDBObject;
import helpers.GlobalHelper;
import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.JacksonDBCollection;
import net.vz.mongodb.jackson.ObjectId;
import net.vz.mongodb.jackson.WriteResult;
import play.Logger;
import play.modules.mongodb.jackson.MongoDB;

import java.util.*;

public class Comment extends MongoDbModel{

	@Id
	@ObjectId
	private String id;
	
	private String comparisonId;
	private String content;
	private String createUserId;
	private Date createTime;
	private Date editTime;
    private Map<String,Object> expandFields;

    @JsonIgnore
    private Map<String,Object> fields;

    public Comment() {
        this.createTime = new Date();
        this.expandFields = new HashMap<String,Object>();
        this.fields = new HashMap<String,Object>();
    }

	public String getComparisonId() {
        return comparisonId;
	}

	public void setComparisonId(String comparisonId) {
        this.comparisonId = comparisonId;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getEditTime() {
		return editTime;
	}

	public void setEditTime(Date editTime) {
		this.editTime = editTime;
	}

    public Map<String, Object> getExpandFields() {
        return expandFields;
    }

    public void setExpandFields(Map<String, Object> expandFields) {
        this.expandFields = expandFields;
    }

    @Override
    public Map<String, Object> getFields() { return fields; }

    private static JacksonDBCollection<Comment, String> collection = MongoDB.getCollection("comment", Comment.class, String.class);

    public static JacksonDBCollection<Comment, String> getCollection() {
        return collection;
    }

    public static Comment getById(String id) {
        try {
            return collection.findOneById(id);
        } catch (Exception e) {
            Logger.error("Fail to get comment by id " + id, e);
        }
        return null;
    }

    public static void create(Comment comment) {
        try {
            if (comment == null) throw new NullPointerException();
            WriteResult<Comment,String> result = collection.insert(comment);
            comment.setId(result.getSavedId());
        } catch (Exception e) {
            Logger.error("Fail to create new comment",e);
        }
    }

    public static Comment update(Comment comment) {
        try {
            if (comment == null) throw new NullPointerException();
            WriteResult<Comment,String> result = collection.save(comment);
            return result.getSavedObject();
        } catch (Exception e) {
            Logger.error("Fail to update comment " + comment.getId(),e);
        }
        return null;
    }

    public static void delete(String id) {
        try {
            Comment comment = collection.findOneById(id);
            if (comment != null) {
                collection.remove(comment);
            }
        } catch (Exception e) {
            Logger.error("Fail to delete comment " + id, e);
        }

    }

    public static void bulkDelete(Map<String,String> queryCriteria) {
        if (queryCriteria == null || queryCriteria.size() == 0) return;
        BasicDBObject query = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
        for (Map.Entry<String, String> entry : queryCriteria.entrySet()) {
            obj.add(new BasicDBObject(entry.getKey(),new BasicDBObject("$regex",entry.getValue()).append("$options", "i")));
        }
        query.put("$and", obj);
        collection.remove(query);
    }

    public static Comment mapJsonToObject(JsonNode json, Comment comment) {
        try {
            if (json == null) throw new NullPointerException();
            if (comment == null) {
                comment = new Comment();
                comment.setComparisonId(json.findPath("comparisonId").asText());
                comment.setContent(json.findPath("content").asText());
                comment.setCreateUserId(json.findPath("createUserId").asText());
            } else {
                if (json.findPath("comparisonId").isTextual())  comment.setComparisonId(json.findPath("comparisonId").asText());
                if (json.findPath("content").isTextual()) comment.setContent(json.findPath("content").asText());
                if (json.findPath("createUserId").isTextual()) comment.setCreateUserId(json.findPath("createUserId").asText());
                comment.setEditTime(new Date());
            }
            return comment;
        } catch (Exception e) {
            Logger.error("Fail to map json data to comment object",e);
        }
        return null;
    }

    @Override
    public void expandRelatedFields(List<String> expandFields){
        if (expandFields == null) return;
        if (this.expandFields == null) {
            this.expandFields = new HashMap<String,Object>();
        }
        for (String field : expandFields) {
            if (field.equals("comparison")) {
                this.expandFields.put("comparison", GlobalHelper.chooseMessageIfObjectNull(Comparison.getById(comparisonId),"Comparison Id " + comparisonId + " is invalid"));
            } else if (field.equals("createUser")) {
                User user = User.getById(createUserId);
                if (user == null) {
                    this.expandFields.put("createUser","User Id " + createUserId + " is invalid");
                } else {
                    user.applySecurity();
                    this.expandFields.put("createUser",user.getFields());
                }
            }
        }

    }

    @Override
    public void buildFields() {
        if (fields == null) {
            fields = new HashMap<String,Object>();
        }
        if (fields.containsKey("id") == false) {
            fields.put("id",id);
            fields.put("comparisonId",comparisonId);
            fields.put("content",content);
            fields.put("createUserId",createUserId);
            fields.put("createTime",createTime);
            fields.put("editTime",editTime);
            fields.put("expand",expandFields);
        }
    }

    @Override
    public void buildIndex() {

    }

}
